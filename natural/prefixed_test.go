package natural

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/thegalabs/go/durations"
)

// This checks that no prefix will be ignore
// if a string has a prefix before in the array it will always be ignored
func TestPrefixedCorpuses(t *testing.T) {
	generator := func(p PrefixedCorpus) func(*testing.T) {
		return func(t *testing.T) {
			for i := len(p) - 1; i > 0; i-- {
				str := p[i].p
				for _, j := range p[0:i] {
					assert.False(t, strings.HasPrefix(str, j.p), "%s is eclipsed by %s", str, j.p)
				}
			}
		}
	}

	t.Run("fr", generator(PrefixedFr().(PrefixedCorpus)))
	t.Run("en", generator(PrefixedEn().(PrefixedCorpus)))
}

func jsonFixture(t *testing.T, name string, out interface{}) {
	wd, err := os.Getwd()
	require.Nil(t, err)
	path := path.Join(wd, "../tests/fixtures/"+name+".json")
	f, err := os.Open(path)
	require.Nil(t, err)
	bytes, err := ioutil.ReadAll(f)
	f.Close()
	require.Nil(t, err)
	require.Nil(t, json.Unmarshal(bytes, &out))
}

func TestPrefixedNumbers(t *testing.T) {
	var numbers map[string]map[string]Number
	jsonFixture(t, "numbers", &numbers)

	generator := func(lang string, corpus Corpus) {
		t.Run(lang, func(t *testing.T) {
			ns := numbers[lang]
			require.NotNil(t, ns)

			pc := corpus.(PrefixedCorpus)

			for k, v := range ns {
				s, err := pc.symbolize(k, stateLetter)
				assert.Nil(t, err)
				assert.Equal(t, v, s)
			}
		})
	}

	generator("en", PrefixedEn())
	generator("fr", PrefixedFr())
}

func TestSymbolsEn(t *testing.T) {
	corpus := PrefixedEn()
	generator := func(str string, symbols ...interface{}) {
		t.Run(str, func(t *testing.T) {
			d, err := corpus.Symbols(str)
			require.Nil(t, err)
			assert.Equal(t, symbols, d)
		})
	}

	generator("10se", Number(10), Second)
	generator("1h30", Number(1), Hour, Number(30))
	generator("1h30min", Number(1), Hour, Number(30), Minute)
	generator("30min", Number(30), Minute)
	generator("half hour", Half, Hour)
	generator("an hour and a half", Number(1), Hour, Plus, Number(1), Half)
}

func TestDuration(t *testing.T) {
	generator := func(str string, expected durations.Duration, symbols ...interface{}) {
		t.Run(str, func(t *testing.T) {
			d, err := Duration(symbols)
			require.Nil(t, err)

			assert.Equal(t, expected, *d)
		})
	}

	generator("1h30", durations.Duration{Hour: 1, Minute: 30}, Number(1), Hour, Number(30))
	generator("1h30min", durations.Duration{Hour: 1, Minute: 30}, Number(1), Hour, Number(30), Minute)
	generator("30min", durations.Duration{Minute: 30}, Number(30), Minute)
	generator("half hour", durations.Duration{Hour: 0.5}, Half, Hour)
	generator("an hour and a half", durations.Duration{Hour: 1.5}, Hour, Half)
}

func completeGenerator(t *testing.T, corpus Corpus) func(string, durations.Duration) {
	return func(str string, expected durations.Duration) {
		t.Run(str, func(t *testing.T) {
			symbols, err := corpus.Symbols(str)
			require.Nil(t, err)
			d, err := Duration(symbols)
			require.Nil(t, err)
			assert.Equal(t, expected, *d)
		})
	}
}

func TestCompleteEn(t *testing.T) {
	generator := completeGenerator(t, PrefixedEn())

	generator("10se", durations.Duration{Second: 10})
	generator("1h30", durations.Duration{Hour: 1, Minute: 30})
	generator("1:30", durations.Duration{Hour: 1, Minute: 30})
	generator("1h30min", durations.Duration{Hour: 1, Minute: 30})
	generator("30min", durations.Duration{Minute: 30})
	generator("half hour", durations.Duration{Hour: 0.5})
	generator("an hour and a half", durations.Duration{Hour: 1.5})
	generator("4 hours 13", durations.Duration{Hour: 4, Minute: 13})
}

func TestCompleteFr(t *testing.T) {
	generator := completeGenerator(t, PrefixedFr())

	generator("10se", durations.Duration{Second: 10})
	generator("1h30", durations.Duration{Hour: 1, Minute: 30})
	generator("1:30", durations.Duration{Hour: 1, Minute: 30})
	generator("1h30min", durations.Duration{Hour: 1, Minute: 30})
	generator("30min", durations.Duration{Minute: 30})
	generator("1 demi heure", durations.Duration{Hour: 0.5})
	generator("une heure et demi", durations.Duration{Hour: 1.5})
	generator("4 heures 13", durations.Duration{Hour: 4, Minute: 13})
}
