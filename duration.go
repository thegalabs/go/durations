// Package durations offers parsing durations to and from strings
package durations

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"strings"
)

var separators = map[byte]bool{
	'Y': true, 'M': true, 'W': true, 'D': true, 'H': true, 'S': true,
}

// Duration holds data about a duration
type Duration struct {
	Year, Month, Week, Day, Hour, Minute, Second float32 `bson:",omitempty"`
}

// ToSeconds naïvely converts a duration to seconds
// Results are naïve because a month does not always have the same amount of days, etc.
func (d Duration) ToSeconds() int64 {
	const secondsInAnHour float32 = 60 * 60
	const secondsInADay = secondsInAnHour * 24
	return int64(
		d.Year*365.25*secondsInADay +
			d.Month*30.5*secondsInADay +
			d.Week*7*secondsInADay +
			d.Day*secondsInADay +
			d.Hour*secondsInAnHour +
			d.Minute*60 +
			d.Second)
}

func write(builder *strings.Builder, value float32, char rune) {
	if value != 0 {
		builder.WriteString(strconv.FormatFloat(float64(value), 'f', -1, 32))
		builder.WriteRune(char)
	}
}

// ToString prints the iso8601 format for the duration
func (d Duration) ToString() string {
	builder := strings.Builder{}
	builder.WriteRune('P')
	write(&builder, d.Year, 'Y')
	write(&builder, d.Month, 'M')
	write(&builder, d.Week, 'W')
	write(&builder, d.Day, 'D')
	builder.WriteRune('T')
	write(&builder, d.Hour, 'H')
	write(&builder, d.Minute, 'M')
	write(&builder, d.Second, 'S')
	return builder.String()
}

// Parse parses an iso8601 duration string
func (d *Duration) Parse(str string) (err error) {
	if !strings.HasPrefix(str, "P") {
		err = errors.New("did not start with P")
		return
	}

	count := len(str)
	index := 1
	if count == index {
		err = errors.New("invalid string len")
		return
	}

	var value float32
	var char byte

	time := false

	for index < count {
		if str[index] == 'T' {
			time = true
			index++
		}

		if value, char, err = extractNextValue(str, &index); err != nil {
			return
		}
		if err = assignValue(d, value, char, time); err != nil {
			return
		}
	}

	return
}

func extractNextValue(str string, index *int) (value float32, char byte, err error) {
	for i := *index; i < len(str); i++ {
		char = str[i]
		if separators[char] {
			stringVal := str[*index:i]

			var tempVal float64
			if tempVal, err = strconv.ParseFloat(stringVal, 32); err != nil {
				return
			}
			*index = i + 1
			value = float32(tempVal)
			return
		}
	}
	err = errors.New("could not find separator before end")
	return
}

func assignValue(d *Duration, value float32, char byte, time bool) error {
	switch char {
	case 'Y':
		d.Year = value
	case 'M':
		if time {
			d.Minute = value
		} else {
			d.Month = value
		}
	case 'W':
		d.Week = value
	case 'D':
		d.Day = value
	case 'H':
		d.Hour = value
	case 'S':
		d.Second = value
	default:
		return fmt.Errorf("invalid char %c", char)
	}
	return nil
}

// MarshalJSON implements the JSONMarshaler interface
func (d Duration) MarshalJSON() ([]byte, error) {
	return json.Marshal(d.ToString())
}

// UnmarshalJSON implements the JSONMarshaler interface
func (d *Duration) UnmarshalJSON(data []byte) error {
	var str string
	if err := json.Unmarshal(data, &str); err != nil {
		return err
	}
	return d.Parse(str)
}

// IsZero returns true if a duration is empty
func (d *Duration) IsZero() bool {
	return d.Year == 0 &&
		d.Month == 0 &&
		d.Week == 0 &&
		d.Day == 0 &&
		d.Hour == 0 &&
		d.Minute == 0 &&
		d.Second == 0
}
