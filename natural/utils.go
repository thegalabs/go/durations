package natural

import (
	"strings"
	"unicode"

	"golang.org/x/text/runes"
	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"
)

type state int

const (
	stateSkip state = iota
	stateLetter
	stateNumber
)

var states = map[rune]state{
	'a': stateLetter,
	'b': stateLetter,
	'c': stateLetter,
	'd': stateLetter,
	'e': stateLetter,
	'f': stateLetter,
	'g': stateLetter,
	'h': stateLetter,
	'i': stateLetter,
	'j': stateLetter,
	'k': stateLetter,
	'l': stateLetter,
	'm': stateLetter,
	'n': stateLetter,
	'o': stateLetter,
	'p': stateLetter,
	'q': stateLetter,
	'r': stateLetter,
	's': stateLetter,
	't': stateLetter,
	'u': stateLetter,
	'v': stateLetter,
	'w': stateLetter,
	'x': stateLetter,
	'y': stateLetter,
	'z': stateLetter,
	':': stateLetter,
	'0': stateNumber,
	'1': stateNumber,
	'2': stateNumber,
	'3': stateNumber,
	'4': stateNumber,
	'5': stateNumber,
	'6': stateNumber,
	'7': stateNumber,
	'8': stateNumber,
	'9': stateNumber,
	'.': stateNumber,
	'/': stateNumber,
}

var normalizer = transform.Chain(norm.NFD, runes.Remove(runes.In(unicode.Mn)), norm.NFC)

func normalize(str string) (string, error) {
	s, _, err := transform.String(normalizer, str)
	if err != nil {
		return "", err
	}
	return strings.ToLower(s), err
}

func parse(str string, symbolizer func(string, state) (interface{}, error)) ([]interface{}, error) {
	state := stateSkip
	index := 0

	var out []interface{}

	for i, b := range str {
		newState := states[b]
		if newState == state {
			continue
		}
		if state != stateSkip {
			parsed, err := symbolizer(str[index:i], state)
			if err != nil {
				return nil, err
			}
			out = append(out, parsed)
		}

		index = i
		state = newState
	}

	if state != stateSkip {
		parsed, err := symbolizer(str[index:], state)
		if err != nil {
			return nil, err
		}
		out = append(out, parsed)
	}

	return out, nil
}
