package durations

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestExtractNextValue(t *testing.T) {
	str := "P12S14.5M50D"

	generator := func(index, expIndex int, expVal float32, expChar byte) func(*testing.T) {
		return func(t *testing.T) {
			temp := index
			val, char, err := extractNextValue(str, &temp)

			assert.Nil(t, err)
			assert.Equal(t, expIndex, temp)
			assert.Equal(t, expVal, val)
			assert.Equal(t, expChar, char)
		}
	}

	t.Run("Start", generator(1, 4, 12, 'S'))
	t.Run("Start", generator(4, 9, 14.5, 'M'))
	t.Run("Start", generator(9, len(str), 50, 'D'))
}

func TestParseDuration(t *testing.T) {
	str := "P1Y2M3W4DT5H6M7S"

	d := &Duration{}

	err := d.Parse(str)
	assert.Nil(t, err)
	assert.Equal(t, float32(1), d.Year)
	assert.Equal(t, float32(2), d.Month)
	assert.Equal(t, float32(3), d.Week)
	assert.Equal(t, float32(4), d.Day)
	assert.Equal(t, float32(5), d.Hour)
	assert.Equal(t, float32(6), d.Minute)
	assert.Equal(t, float32(7), d.Second)
}

func TestToSeconds(t *testing.T) {
	generator := func(d Duration, expected int64) func(*testing.T) {
		return func(t *testing.T) {
			assert.Equal(t, expected, d.ToSeconds())
		}
	}

	t.Run("Full", generator(Duration{1, 2, 3, 4, 5, 6, 7}, 39006368))
	t.Run("30min", generator(Duration{Minute: 30}, 1800))
	t.Run("0.5h", generator(Duration{Hour: 0.5}, 1800))
	t.Run("Seconds", generator(Duration{Second: 7}, 7))
}

func TestParseSanity(t *testing.T) {
	generator := func(input string) func(*testing.T) {
		return func(t *testing.T) {
			d := &Duration{}
			err := d.Parse(input)
			assert.Nil(t, err)

			str := d.ToString()

			assert.Equal(t, input, str)
		}
	}

	t.Run("PT30M", generator("PT30M"))
	t.Run("P1Y2M3W4.5DT5H6.1M7S", generator("PT30M"))
}

func TestJSONMarshal(t *testing.T) {
	first := Duration{Month: 1, Hour: 2}

	b, err := json.Marshal(first)
	require.Nil(t, err)
	assert.Equal(t, "\"P1MT2H\"", string(b))

	var d Duration
	err = json.Unmarshal(b, &d)
	require.Nil(t, err)
	assert.Equal(t, first, d)
}

// func TestBSONMarshal(t *testing.T) {
// 	first := Duration{Month: 1, Hour: 2}

// 	b, err := first.MarshalBSON()
// 	require.Nil(t, err)
// 	assert.Equal(t, "\"P1MT2H\"", string(b))

// 	var d Duration
// 	err = d.UnmarshalBSON(b)
// 	require.Nil(t, err)
// 	assert.Equal(t, first, d)
// }

func TestIsZero(t *testing.T) {
	assert.True(t, (&Duration{}).IsZero())
	assert.False(t, (&Duration{Year: 1}).IsZero())
	assert.False(t, (&Duration{Month: 1}).IsZero())
	assert.False(t, (&Duration{Week: 1}).IsZero())
	assert.False(t, (&Duration{Day: 1}).IsZero())
	assert.False(t, (&Duration{Hour: 1}).IsZero())
	assert.False(t, (&Duration{Minute: 1}).IsZero())
	assert.False(t, (&Duration{Second: 1}).IsZero())
}
