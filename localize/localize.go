// Package localize handles converting a duration to a string
package localize

import (
	"fmt"

	"gitlab.com/thegalabs/go/durations"
)

// Localizer converts a duration into a localized
type Localizer interface {
	ToString(*durations.Duration) string
}

// Localizers holds multiple languages
type Localizers map[string]Localizer

// ToString returns a localized string based on the registered localizer
func (l Localizers) ToString(lang string, duration *durations.Duration) (string, error) {
	loc := l[lang]
	if loc == nil {
		return "", fmt.Errorf("no localizer for language %s", lang)
	}
	return loc.ToString(duration), nil
}
