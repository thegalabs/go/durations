// Package natural helps parsing durations
package natural

import (
	"errors"

	"gitlab.com/thegalabs/go/durations"
)

// Unit represents a unit of time
type Unit int

// All possible units
const (
	Year Unit = iota
	Month
	Week
	Day
	Hour
	Minute
	Second
)

// Modifier is a length of time that applies to the last unit
type Modifier int

// All possible modifiers
const (
	Half Modifier = iota
	Quarter
	Plus
	Minus
)

// Number is the symbol for numbers
type Number float32

// Corpus holds data about translating text to string
type Corpus interface {
	Symbols(string) ([]interface{}, error)
}

// Parser has the functions
type Parser map[string]Corpus

// Duration parses a duration from natural text
func (p Parser) Duration(lang, raw string) (*durations.Duration, error) {
	corpus := p[lang]
	if corpus == nil {
		return nil, ErrLangNotSupported
	}
	sym, err := corpus.Symbols(raw)
	if err != nil {
		return nil, err
	}
	return Duration(sym)
}

// ErrLangNotSupported is returned when the language is not supported
var ErrLangNotSupported = errors.New("lang not supported")
