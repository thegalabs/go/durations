package localize

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/thegalabs/go/durations"
)

func simpleGenerator(t *testing.T, simple *SimpleLocalizer) func(string, durations.Duration) {
	return func(expected string, d durations.Duration) {
		t.Run(expected, func(t *testing.T) {
			assert.Equal(t, expected, simple.ToString(&d))
		})
	}
}

func TestSimpleFr(t *testing.T) {
	generator := simpleGenerator(t, SimpleFr())

	generator("1h30", durations.Duration{Hour: 1, Minute: 30})
	generator("30min", durations.Duration{Minute: 30})
}
