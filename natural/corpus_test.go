package natural

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestNormalize(t *testing.T) {
	generator := func(input string, expected string) {
		t.Run(input, func(t *testing.T) {
			s, err := normalize(input)
			assert.Nil(t, err)
			assert.Equal(t, expected, s)
		})
	}

	generator("Caméra", "camera")
	generator("½", "½")
}

func TestParse(t *testing.T) {
	generator := func(input string, expected []string) {
		t.Run(input, func(t *testing.T) {
			p, err := parse(input, func(str string, _ state) (interface{}, error) {
				return str, nil
			})
			assert.Nil(t, err)
			require.Len(t, p, len(expected))
			for i := range expected {
				assert.Equal(t, expected[i], p[i])
			}
		})
	}

	generator("1m30s", []string{"1", "m", "30", "s"})
	generator("1 minute 30 secondes", []string{"1", "minute", "30", "secondes"})
	generator("1/2 hour", []string{"1/2", "hour"})
	generator("1m30s", []string{"1", "m", "30", "s"})
	generator("1 minute 30 secondes", []string{"1", "minute", "30", "secondes"})
	generator("1.5 hour", []string{"1.5", "hour"})
}
