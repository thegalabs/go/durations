# Durations

Dealing with durations in golang

## Features

- a `Duration` struct with explicit components that can be marshalled to and from JSON
- convert to and from [iso8601](https://en.wikipedia.org/wiki/ISO_8601#Durations).
- parse raw text like `an hour and a half` usng the natural package 
- convert a Duration to a localized string using the localize package
