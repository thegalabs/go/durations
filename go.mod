module gitlab.com/thegalabs/go/durations

go 1.15

require (
	github.com/stretchr/testify v1.4.0
	golang.org/x/text v0.3.5
)
