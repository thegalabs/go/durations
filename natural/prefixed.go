package natural

import (
	"errors"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/thegalabs/go/durations"
)

type prefix struct {
	p string
	s interface{}
}

// PrefixedCorpus tries to use the beginning of words to match them
type PrefixedCorpus []prefix

func (c PrefixedCorpus) find(str string) (interface{}, error) {
	for _, p := range c {
		if strings.HasPrefix(str, p.p) {
			return p.s, nil
		}
	}
	return nil, fmt.Errorf("could not match str '%s'", str)
}

func (c PrefixedCorpus) symbolize(str string, s state) (interface{}, error) {
	switch s {
	case stateLetter:
		return c.find(str)
	case stateNumber:
		f, err := strconv.ParseFloat(str, 32)
		if err != nil {
			return nil, err
		}
		return Number(f), nil
	default:
		return nil, errors.New("unrecognized state")
	}
}

// Symbols implements the corpus interface
func (c PrefixedCorpus) Symbols(str string) ([]interface{}, error) {
	return parse(str, c.symbolize)
}

func pointer(duration *durations.Duration, unit Unit) (pt *float32, err error) {
	switch unit {
	case Year:
		pt = &(duration.Year)
	case Month:
		pt = &(duration.Month)
	case Week:
		pt = &(duration.Week)
	case Day:
		pt = &(duration.Day)
	case Hour:
		pt = &(duration.Hour)
	case Minute:
		pt = &(duration.Minute)
	case Second:
		pt = &(duration.Second)
	default:
		err = fmt.Errorf("no pointer for unit %d", unit)
	}
	return
}

// Duration translates a list of symbols into a duration
func Duration(symbols []interface{}) (duration *durations.Duration, err error) {
	duration = &durations.Duration{}
	var pt *float32
	var acc float32
	var unit Unit

	for _, sym := range symbols {
		switch s := sym.(type) {
		case Number:
			acc += float32(s)
		case Unit:
			unit = s
			if pt, err = pointer(duration, unit); err != nil {
				return
			}
			if acc != 0 {
				*pt = acc
				acc = 0
			} else {
				*pt = 1
			}
		case Modifier:
			switch s {
			case Half:
				acc = 0.5
			case Quarter:
				acc = 0.25
			case Plus:
				break
			case Minus:
				err = errors.New("unexecpted minus")
			}
		default:
			err = fmt.Errorf("unsupported symbol %s", s)
		}
	}
	if acc != 0 {
		var newUnit Unit
		switch symbols[len(symbols)-1].(type) {
		case Number:
			newUnit = unit + 1
		case Modifier:
			newUnit = unit
		}
		if pt, err = pointer(duration, newUnit); err != nil {
			return
		}
		*pt += acc
	}

	return
}
