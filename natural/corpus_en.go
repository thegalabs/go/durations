package natural

// PrefixedEn returns a Prefixed corpus for the english language
func PrefixedEn() Corpus {
	return PrefixedCorpus{
		{"z", Number(0)},
		{"on", Number(1)},
		{"two", Number(2)},
		{"thirty", Number(30)},
		{"thir", Number(13)},
		{"th", Number(3)},
		{"sixty", Number(60)},
		{"forty", Number(40)},
		{"fourte", Number(14)},
		{"four", Number(4)},
		{"fifty", Number(50)},
		{"fifte", Number(15)},
		{"fi", Number(5)},
		{"sixte", Number(16)},
		{"si", Number(6)},
		{"sevent", Number(17)},
		{"sev", Number(7)},
		{"eighte", Number(18)},
		{"ei", Number(8)},
		{"ninet", Number(19)},
		{"ni", Number(9)},
		{"te", Number(10)},
		{"el", Number(11)},
		{"twen", Number(20)},
		{"tw", Number(12)},
		{"½", Half},
		{"1/2", Half},
		{"1/4", Quarter},
		{"quar", Quarter},
		{"hal", Half},
		{"and", Plus},
		{"minus", Minus},
		{"a", Number(1)}, // valid for a and an
		{"h", Hour},
		{"m", Minute},
		{"s", Second},
		{":", Hour},
	}
}
