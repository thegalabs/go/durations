package localize

import (
	"strconv"
	"strings"

	"gitlab.com/thegalabs/go/durations"
)

// SimpleLocalizer just concatenates strings
// Unit strings are ordered the same as the Duration struct:
// Year, Month, Week, Day, Hour, Minute, Second
type SimpleLocalizer struct {
	Singulars [7]string
	Plurals   [7]string
	// OmitLast should return true if the last unit can be omitted
	OmitLast func(*durations.Duration, int) bool
}

// reduce converts a duration to a sparse array of values
func reduce(d *durations.Duration) (indices []int, values []float32) {
	// No pointer arithmetic in golang :(
	list := [7]float32{d.Year, d.Month, d.Week, d.Day, d.Hour, d.Minute, d.Second}
	for i, v := range list {
		if v > 0 {
			indices = append(indices, i)
			values = append(values, v)
		}
	}
	return
}

// ToString implements the localizer protocol
func (s *SimpleLocalizer) ToString(d *durations.Duration) string {
	indices, values := reduce(d)
	if len(indices) == 0 {
		return ""
	}

	builder := strings.Builder{}
	writer := func(i int) {
		index := indices[i]
		value := values[i]

		builder.WriteString(strconv.FormatFloat(float64(value), 'f', -1, 32))

		if value > 1 {
			builder.WriteString(s.Plurals[index])
		} else {
			builder.WriteString(s.Singulars[index])
		}
	}

	if len(indices) == 1 {
		writer(0)
		return builder.String()
	}

	for i := 0; i < len(indices)-1; i++ {
		writer(i)
	}

	index := len(indices) - 1

	if s.OmitLast != nil && s.OmitLast(d, indices[index]) {
		builder.WriteString(strconv.FormatFloat(float64(values[index]), 'f', -1, 32))
	} else {
		writer(index)
	}
	return builder.String()
}

// SimpleFr returns a simple localizer for france
func SimpleFr() *SimpleLocalizer {
	return &SimpleLocalizer{
		Singulars: [7]string{" an", " mois", " semaine", " jour", "h", "min", "s"},
		Plurals:   [7]string{" ans", " mois", " semaines", " jours", "h", "min", "s"},
		OmitLast: func(d *durations.Duration, i int) bool {
			return i == 5 && d.Hour != 0 || i == 6 && d.Minute != 0
		},
	}
}

// SimpleEn returns a simple localizer for france
func SimpleEn() *SimpleLocalizer {
	return &SimpleLocalizer{
		Singulars: [7]string{" year", " month", " week", " day", "h", "m", "s"},
		Plurals:   [7]string{" years", " months", " weeks", " days", "h", "m", "s"},
		OmitLast: func(d *durations.Duration, i int) bool {
			return i == 5 && d.Hour != 0 || i == 6 && d.Minute != 0
		},
	}
}
